#!/usr/bin/env python
import sys

from fcg.io.cli import Cli


cli = Cli()
cli.start(sys.argv[1:])
