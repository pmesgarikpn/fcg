# requirements
# Given I run the command ./snippet.py without specifying any arguments then I see an error message saying
# "missing input arguments"
# Given I run the command ./snippet.py --template=wtp.template --source=./data/input.csv then an output file
# is generated at ./data/output.txt
# Given I run the command ./snippet.py --template=wtp.template --source=./data/input.csv --filter.location=loc1
# then an output file is generated for loc1 configuration at ./data/loc1/output.txt
# Given I run the command ./snippet.py --template=wtp.template --source=./data/input.csv --filter.location=loc1
# if loc1 is not found then I see an error message saying "loc1 can not be found in the source"

import unittest
import os
from jinja2 import FileSystemLoader, Template
from fcg.core.config_generator import TemplateManager, ConfigGenerator, \
    FilePresenter, TxtWriter, CsvFileLoader, DataManager, DictLoader
from fcg.io.cli import Cli

base_dir = os.path.abspath(os.path.dirname(__file__))


class CliParserTest(unittest.TestCase):
    """
    Requirements:
        Cli parser returns a dict of all parsed arguments
    """
    def setUp(self) -> None:
        pass

    def tearDown(self) -> None:
        pass

    def test_parser_requires_template_and_source_as_arguments(self):
        cli = Cli()

        # without filter
        result_1 = cli.parse_args(["--template=mytemp.j2", "--data=input.csv"])
        assert result_1['template'] == 'mytemp.j2'
        assert result_1['data'] == 'input.csv'

        # with filter
        result_2 = cli.parse_args(["--template=mytemp.j2", "--data=input.csv", "--filter.location=loc-1", "--filter.model=1"])
        assert result_2['template'] == 'mytemp.j2'
        assert result_2['data'] == 'input.csv'
        assert result_2['filter']['location'] == 'loc-1'
        assert result_2['filter']['model'] == '1'


class TemplateManagerTest(unittest.TestCase):
    """
    Requirements:
        Template manager can be initialized by specifying a loader for templates
        Template manager can load a template given a template name
        Template manager can render a template
    """

    def setUp(self) -> None:
        # for now just test file system loader
        loader = FileSystemLoader(os.path.join(base_dir, 'templates'))
        self.tm = TemplateManager(loader)

    def tearDown(self) -> None:
        pass

    def test_template_manager_can_load_a_template(self):
        self.tm.set_template_name('WTP.j2')
        self.tm.load()
        template = self.tm.get_template()
        assert isinstance(template, Template)

    def test_template_manager_can_render_a_template_using_load(self):
        self.tm.set_template_name('WTP.j2')
        self.tm.load()
        data = {'serial_number': 'XXYY'}
        result = self.tm.render(data)
        assert 'edit XXYY' in result

    def test_template_manager_can_render_a_template_using_template_constructor(self):
        template = Template('edit {{ serial_number }}')
        self.tm.set_template(template)
        data = {'serial_number': '20134'}
        result = self.tm.render(data)
        assert 'edit 20134' in result


class DataManagerTest(unittest.TestCase):
    def setUp(self) -> None:
        pass

    def tearDown(self) -> None:
        pass

    def test_csv_file_loader_without_query(self):
        csv_loader = CsvFileLoader()
        csv_loader.set_filename('sample.csv')
        data = csv_loader.load()

        assert len(data) == 3

    def test_csv_file_loader_with_query(self):
        csv_loader = CsvFileLoader()
        csv_loader.set_filename('sample.csv')
        csv_loader.set_query({'Serienummer': 'FP221ETF18041191'})
        data_1 = csv_loader.load()

        assert len(data_1) == 1

        csv_loader.set_filename('sample.csv')
        csv_loader.set_query({'Locatie Code': 'COPLOC2110-Velp'})
        data_2 = csv_loader.load()
        assert len(data_2) == 2

    def test_dict_loader_without_query(self):
        d_1 = [
            {
                'serial_number': 'XXYY',
                'location': 'loc-1',
                'model': 'X1'
            },
            {
                'serial_number': '201AB',
                'location': 'loc-1',
                'model': 'X1'
            },
            {
                'serial_number': 'RTY12',
                'location': 'loc-2',
                'model': 'X2'
            }
        ]
        dict_loader = DictLoader(d_1)
        data_1 = dict_loader.load()

        assert len(data_1) == 3

    def test_dict_loader_with_query(self):
        d_1 = [
            {
                'serial_number': 'XXYY',
                'location': 'loc-1',
                'model': 'X1'
            },
            {
                'serial_number': '201AB',
                'location': 'loc-1',
                'model': 'X1'
            },
            {
                'serial_number': 'RTY12',
                'location': 'loc-2',
                'model': 'X2'
            }
        ]
        dict_loader = DictLoader(d_1)
        dict_loader.set_query({'location': 'loc-1'})
        data_1 = dict_loader.load()

        assert len(data_1) == 2


class ConfigGeneratorTest(unittest.TestCase):
    """
    Requirements:
        Config generator can be initialized by specifying a template manager, a data object, and a presenter
        Config generator can generate configuration snippets for all items in data object
        Config generator can generate configuration snippets for a certain item in data object when a filter is given
    """

    def setUp(self) -> None:
        self.dm = DataManager()
        self.tm = TemplateManager()
        self.cg = ConfigGenerator(data_manager=self.dm, template_manager=self.tm)

    def tearDown(self) -> None:
        pass

    def test_config_generator_can_generate_config_without_filter(self):
        template = Template('config wireless-controller wtp\nedit {{ serial_number }}')
        data = [
            {
                'serial_number': 'XXYY',
                'location': 'loc-1',
                'model': 'X1'
            },
            {
                'serial_number': '201AB',
                'location': 'loc-1',
                'model': 'X1'
            },
            {
                'serial_number': 'RTY12',
                'location': 'loc-2',
                'model': 'X2'
            }
        ]
        dict_loader = DictLoader(data)
        self.dm.set_loader(dict_loader)
        self.tm.set_template(template)
        result = self.cg.generate()
        assert len(result) == 3
        assert 'edit XXYY' in result[0]
        assert 'edit 201AB' in result[1]
        assert 'edit RTY12' in result[2]

    def test_config_generator_can_generate_config_with_filter(self):
        template = Template('config wireless-controller wtp\nedit {{ serial_number }}')
        data = [
            {
                'serial_number': 'XXYY',
                'location': 'loc-1',
                'model': 'X1'
            },
            {
                'serial_number': '201AB',
                'location': 'loc-1',
                'model': 'X1'
            },
            {
                'serial_number': 'RTY12',
                'location': 'loc-2',
                'model': 'X2'
            }
        ]
        dict_loader = DictLoader(data)
        dict_loader.set_query({'location': 'loc-1'})
        self.dm.set_loader(dict_loader)
        self.tm.set_template(template)
        result = self.cg.generate()
        assert len(result) == 2
        assert 'edit XXYY' in result[0]
        assert 'edit 201AB' in result[1]

        dict_loader.set_query({'location': 'loc-1', 'serial_number': '201AB'})
        result = self.cg.generate()
        assert len(result) == 1
        assert 'edit 201AB' in result[0]

        dict_loader.set_query({'location': '3'})
        result = self.cg.generate()
        assert len(result) == 0

    def test_config_generator_can_output_txt_file(self):
        template = Template('config wireless-controller wtp\nedit {{ serial_number }}')
        data = [
            {
                'serial_number': 'XXYY',
                'location': 'loc-1',
                'model': 'X1'
            },
            {
                'serial_number': '201AB',
                'location': 'loc-1',
                'model': 'X1'
            },
            {
                'serial_number': 'RTY12',
                'location': 'loc-2',
                'model': 'X2'
            }
        ]
        dict_loader = DictLoader(data)
        self.dm.set_loader(dict_loader)
        self.tm.set_template(template)
        writer = TxtWriter()
        fp = FilePresenter(writer=writer)
        self.cg.set_presenter(fp)

        result = self.cg.output()
        assert 'output.txt' in result


class CsvFileLoaderTest(unittest.TestCase):
    def setUp(self) -> None:
        pass

    def tearDown(self) -> None:
        pass

    def test_load_csv_file_default_directory(self):
        filename = 'sample.csv'
        csv_loader = CsvFileLoader()
        csv_loader.set_filename(filename)
        result_1 = csv_loader.load()
        assert len(result_1) == 3


if __name__ == '__main__':
    unittest.main()
