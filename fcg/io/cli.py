import argparse
import os
from jinja2 import FileSystemLoader
from fcg.core.config_generator import TemplateManager, ConfigGenerator, FilePresenter, \
    TxtWriter, DataManager, CsvFileLoader

base_dir = os.path.abspath(os.path.dirname(__file__))


class Cli:
    def __init__(self):
        self.parser = argparse.ArgumentParser()

    def parse_args(self, args):
        result = {}
        for arg in args:
            if arg.startswith('--'):
                arg = arg[2:]
                # --template=mytemp.j2
                if '=' in arg:
                    key, value = arg.split('=')
                    # --filter.location=myloc
                    if '.' in key:
                        # key=filter.location
                        # value=myloc
                        splitted = key.split('.')
                        key = splitted[0]
                        value = {splitted[1]: value}
                else:
                    value = True
                    key = arg
                if key in result:
                    value = {**result[key], **value}
                result[key] = value
        return result

    def start(self, args=None):
        if args is None:
            args = []
        parsed_args = self.parse_args(args)
        data_loader = None
        query = None
        if 'filter' in parsed_args:
            query = parsed_args['filter']
        if 'csv' in parsed_args:
            data_loader = CsvFileLoader()
            data_loader.set_filename(parsed_args['csv'])
            data_loader.set_query(query)
        if 'template' not in parsed_args:
            raise Exception('template is missing')
        template = parsed_args['template']

        writer = TxtWriter()
        presenter = FilePresenter(writer=writer)

        template_loader = FileSystemLoader(
            os.path.join(os.path.abspath(os.path.dirname(os.path.dirname(base_dir))), 'templates'))
        tm = TemplateManager(template_loader)
        tm.set_template_name(template)
        dm = DataManager(data_loader)
        cg = ConfigGenerator(template_manager=tm, data_manager=dm, presenter=presenter)
        cg.generate()
        cg.output()
