import os
import csv
from jinja2 import Environment

base_dir = os.path.abspath(os.path.dirname(__file__))


class TemplateManager:
    def __init__(self, loader=None):
        if loader:
            self.env = Environment(loader=loader)
        self.template = None
        self.template_name = None

    def set_template_name(self, template_name):
        self.template_name = template_name

    def load(self):
        if not self.template:
            self.template = self.env.get_template(self.template_name)

    def set_template(self, template):
        self.template = template

    def get_template(self):
        return self.template

    def render(self, data):
        return self.template.render(**data)


class FilePresenter:
    def __init__(self, output_path=None, writer=None):
        self.output_path = output_path
        self.writer = writer

        if not self.output_path:
            self.output_path = os.path.join(
                os.path.abspath(os.path.dirname(os.path.dirname(base_dir))), 'data', 'output')
        if not os.path.exists(self.output_path):
            os.makedirs(self.output_path)

    def set_output_path(self, path):
        self.output_path = path

    def set_writer(self, writer):
        self.writer = writer

    def present(self, data):
        if not self.writer:
            raise Exception('No writer has been defined. Can not write.')
        return self.writer.write(data, self.output_path)


class TxtWriter:
    def __init__(self):
        self.extension = 'txt'
        self.file_name = 'output'

    def to_string(self, data):
        return '\n'.join(data)

    def write(self, data, output_path):
        storage_location = os.path.join(output_path, '{}.{}'.format(self.file_name, self.extension))
        with open(storage_location, 'w') as f:
            f.write(self.to_string(data))
        return 'writer wrote to {}'.format(storage_location)


class DataManager:
    def __init__(self, loader=None):
        self.loader = loader

    def set_loader(self, loader):
        self.loader = loader

    def load(self):
        return self.loader.load()


class DictLoader:
    def __init__(self, data):
        self.data = data
        self.query = None

    def set_query(self, query):
        self.query = query

    def run_query(self, data, args):
        result = []

        def matches(item, args):
            for key, value in args.items():
                if item[key] != value:
                    return False
            return True

        for item in data:
            if matches(item, args):
                result.append(item)
        return result

    def load(self):
        result = self.data
        if self.query:
            result = self.run_query(self.data, self.query)
        return result


class CsvFileLoader:
    def __init__(self, search_path=None):
        self.search_path = search_path
        if not self.search_path:
            self.search_path = os.path.join(os.path.abspath(os.path.dirname(os.path.dirname(base_dir))), 'data')
        self.filename = None
        self.query = None

    def set_filename(self, filename):
        self.filename = filename

    def set_query(self, query):
        self.query = query

    def run_query(self, data, args):
        result = []

        def matches(item, args):
            for key, value in args.items():
                if item[key] != value:
                    return False
            return True

        for item in data:
            if matches(item, args):
                result.append(item)
        return result

    def load(self):
        result = []
        with open(os.path.join(self.search_path, self.filename)) as f:
            reader = csv.DictReader(f)
            for row in reader:
                result.append(row)
        if self.query:
            result = self.run_query(result, self.query)
        return result


class ConfigGenerator:
    def __init__(self, template_manager=None, data_manager=None, presenter=None):
        self.template_manager = template_manager
        self.data_manager = data_manager
        self.presenter = presenter

    def set_template_manager(self, template_manager):
        self.template_manager = template_manager

    def set_data_manager(self, data_manager):
        self.data_manager = data_manager

    def set_presenter(self, presenter):
        self.presenter = presenter

    def generate(self):
        config = []

        self.template_manager.load()
        data = self.data_manager.load()
        if not data:
            return config
        for item in data:
            config.append(self.template_manager.render(item))
        return config

    def output(self):
        return self.presenter.present(self.generate())

