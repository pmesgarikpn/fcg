## Fortinet Configuration Generator (fcg)
fcg is a Python tool to generate configuration snippets. Its main features are:

* Supports jinja2 templates
* Uses a CSV file as input
* Allows user to specify a filter on the input

## Setup
To start using fcg, simply download the package, and install the requirements.
It is advised to use a virtual environment to use fcg.

1. Download the package
1. Unzip the package in your directory of choice
    > After unzipping the package, your directory structure should look like this:
    
        .                          # root directory
        ├── fcg                    # core files
        ├── .gitignore.py            
        ├── __init__.py            
        ├── fcg_cli.py             # cli interface
        ├── README.md              
        ├── requirements.txt
        └── tests.py               # tests
        
1. In the root package, create a directory called `data`. This is the location where you can put
your csv files as input. This is also the location where the generated configuration is stored.
1. In the root package, create a directory called `templates`. This is the location where you can
put your jinja2 templates.

    > At the end of this step, your directory structure should look like this:
                                                                                                                                 
        .                          # root directory
        |── data                   # data files, both input and output
        ├── fcg                    # core files
        ├── templates              # template files
        ├── .gitignore.py            
        ├── __init__.py            
        ├── fcg_cli.py             # cli interface
        ├── README.md              
        ├── requirements.txt
        └── tests.py               # tests

1. Within the root directory, run the command below to create a new virtual environment:
    > Linux: 
    
    `python3 -m venv ./venv`
    
    > Windows: 
    
    `c:\c:\Python3\python -m venv venv`
    
    > Now your directory structure should look like this:
    
        .                          # root directory
        |── data                   # data files, both input and output
        ├── fcg                    # core files
        ├── templates              # template files
        ├── venv                   # the Python virtual environment
        ├── .gitignore.py
        ├── __init__.py            
        ├── fcg_cli.py             # cli interface
        ├── README.md              
        ├── requirements.txt
        └── tests.py               # tests
    
1. Within the root directory, run the command below to install the requirements:

    `pip install -r requirements.txt`
 
## Usage
fcg works with any template supporting Jinja2 syntax. Although currently only string interpolation
is supported, via the double curly brackets syntax `{{ ... }}`.

To generate config, two input parameters are required:
* template name
* csv file name

To use a template, simply put the template file with extension `.j2` in the `templates` directory.
To use a csv file, simply put the csv file in the `data` directory.

Once your template file and csv file are located in the correct directories, you can use the
following command samples to generate a configuration file.

It is important to use the virtual environment Python interpreter or make sure the requirements
are installed in the system wide interpreter if you don't wish to use a virtual environment.

For the sample commands below, we assume you are using the virtual environment Python interpreter,
created in step 3. You can activate the environment from cli using commands below:

> Linux

`source venv/Scripts/activate`

> Windows

`venv\Scripts\activate.bat`


### Examples
Make sure you are running these command while being in the root directory.

> Generate configurations using `WTP.j2` template and `sample.csv` as input 

`fcg_cli.py --template=WTP.j2 --csv=sample.csv`       

> Generate configurations using `WTP.j2` template and `sample.csv` as input and filter
>the input for location with `locatie_code=LOC2110`

`fcg_cli.py --template=WTP.j2 --csv=sample.csv --filter.locatie_code=LOC2110`

> Generate configuration using `WTP.j2` template and `sample.csv` as input and filter
>the input for location with `locatie_code=LOC2110` and serial number `serial_number=FP221ETF18041099`

`fcg_cli.py --template=WTP.j2 --csv=sample.csv --filter.locatie_code=LOC2110 --filter.serial_number=FP221ETF18041099`